import json
import requests

def get_ykccn_token():
    url = f'https://www.ykccn.com/ompApi/api/omp/cpo/privilege/account/login'
    # 设置请求头
    headers = {
        "Host": "www.ykccn.com",
        "Connection": "keep-alive",
        "Cipher-Type": "1",
        "Authorization": "YKC-AUTHORIZATION",
        "Content-Type": "application/json",
        "Accept": "application/json, text/plain, */*",
        "Security_Code": "1718258831304",
        "Origin": "https://www.ykccn.com",
        "Referer": "https://www.ykccn.com/OMP",
    }
    # 定义请求体内容
    body = (
        "KAlPOgfdBZk6fnZIGOtTbyx1Ylr0HLBjJEIK+NvY5nl6p4pnCBRdu8aW94nCdZBHajmnE4Vpynqwm1nI67l5Ov5VCiqzT0D+XLI7LQI1uF1o/EYaV3Q+/QmQiYUm+4GrZ9wfHD9mQCXwJiw6tf413TYygswCxT3QIuxj+8qhpx8PXIeUbEi1PvFtzYgeIP1LFMEbjW3KoE1deswfYoAUcCNK0XtTjhCMKf6AeVmy6G6d1qBcOO1UDWUaVrbW6C0GtBjz8iCQNLb2Kgcp7YP4T7KNprOeY7WU6jPkwLPL9XEBrrL9KHNpM6C3/xtXzhUDv6DpObv+l7Cx3XEXloogy9bz1nV87M12A8065gQls+DegdVVdzOQ9RzQimbh2/ho+3gAVqAnQAHvo23tUlY507WmoC+nSY8NDFSaWaZEa4ZZKsn32EUz9YWLA9ffBXUMHaVBpgTJQVfH4WTjUp47AgtxJAXMbOHNehCGBRJTfly7km5kHEbtMXyLx5w/8kHM"
    )

    # 发送POST请求
    try:
        response = requests.post(url, headers=headers, data=body)
        response.raise_for_status()  # 检查请求是否成功
        data = response.json()  # 解析响应的JSON数据
        # print("Success:", data)
    # 获取token
        getToken = f"{data['body']['accountId']},{data['header']['token']}"
        return getToken
        # print("Token:", getToken)
    except requests.exceptions.RequestException as e:
        print("Error:", e)

if __name__ == '__main__':
    ykccn_access_token = get_ykccn_token()
    print(ykccn_access_token)

