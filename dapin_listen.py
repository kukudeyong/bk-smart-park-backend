import time
from flask import Flask, jsonify,request
from flask_cors import CORS
import dapinapi
import datetime
import threading

# log = logging.getLogger('werkzeug')
# log.setLevel(logging.ERROR)
app = Flask(__name__)
CORS(app)
app.config["SECRET_KEY"] = 'bkjd8369'

try:
    print("初始化钉钉公告信息...")
    ding_information_cache = dapinapi.api_ding_information()
except Exception as e:
    print(f"初始化钉钉公告信息失败: {e}")
    ding_information_cache = None

try:
    print("初始化充电桩状态信息...")
    chargingStation_cache = dapinapi.api_chargingStation()
except Exception as e:
    print(f"初始化充电桩状态信息失败: {e}")
    chargingStation_cache = None


try:
    print("初始化大华报警信息...")
    dahua_alarm_cache = dapinapi.api_dahua_alarm()
except Exception as e:
    print(f"初始化大华报警信息失败: {e}")
    dahua_alarm_cache = None

try:
    print("初始化钉钉考勤信息...")
    ding_attendance_cache = dapinapi.api_ding_attendance()
except Exception as e:
    print(f"初始化钉钉考勤信息失败: {e}")
    ding_attendance_cache = None

try:
    print("初始化访客信息...")
    visitor_cache = dapinapi.api_visitor()
except Exception as e:
    print(f"初始化访客信息失败: {e}")
    visitor_cache = None

try:
    print("初始化车库信息...")
    garage_cache = dapinapi.api_garage()
except Exception as e:
    print(f"初始化车库信息失败: {e}")
    garage_cache = None


def is_time(start, end):
    # 范围时间
    start_time = datetime.datetime.strptime(str(datetime.datetime.now().date()) + start, '%Y-%m-%d%H:%M')
    end_time = datetime.datetime.strptime(str(datetime.datetime.now().date()) + end, '%Y-%m-%d%H:%M')
    # 当前时间
    now_time = datetime.datetime.now()
    # 判断当前时间是否在范围时间内
    if start_time < now_time < end_time:
        return 1  # 在这个范围内
    else:
        return 0


def update_ding_information_cache():
    global ding_information_cache
    global ding_attendance_cache
    while True:
        try:
            now_time = datetime.datetime.now()
            start = "8:00"
            end = "9:00"
            up = is_time(start, end)
            if up == 1:
                print(f"{now_time},更新钉钉公告线程启动...")
                ding_information_cache = dapinapi.api_ding_information()
                print(f"{now_time},更新钉钉考勤线程启动...")
                ding_attendance_cache = dapinapi.api_ding_attendance()
        except BaseException:
            pass
        time.sleep(30 * 60)


def update_ten_min_cache():
    global dahua_alarm_cache
    global visitor_cache
    global garage_cache
    global chargingStation_cache
    while True:
        try:
            now_time = datetime.datetime.now()
            start = "7:00"
            end = "20:00"
            up = is_time(start, end)
            if up == 1:
                print(f"{now_time},更新大华报警线程启动...")
                dahua_alarm_cache = dapinapi.api_dahua_alarm()
                print(f"{now_time},更新访客线程启动...")
                visitor_cache = dapinapi.api_visitor()
                print(f"{now_time},更新车库线程启动...")
                garage_cache = dapinapi.api_garage()
                print(f"{now_time},更新充电桩状态线程启动...")
                chargingStation_cache = dapinapi.api_chargingStation()
        except BaseException:
            pass
        time.sleep(10 * 60)


@app.route('/ding_information')  # 钉钉公告
def ding_information():
    global ding_information_cache
    if ding_information_cache is None:
        ding_information_cache = dapinapi.api_ding_information()
        return jsonify(ding_information_cache)
    else:
        return jsonify(ding_information_cache)

@app.route('/chargingStation')  # 充电桩状态
def chargingStation():
    global chargingStation_cache
    if chargingStation_cache is None:
        chargingStation_cache = dapinapi.api_chargingStation()
        return jsonify(chargingStation_cache)
    else:
        return jsonify(chargingStation_cache)


@app.route('/dahua_alarm')  # 大华报警
def dahua_alarm():
    global dahua_alarm_cache
    if dahua_alarm_cache is None:
        dahua_alarm_cache = dapinapi.api_dahua_alarm()
        return jsonify(dahua_alarm_cache)
    else:
        return jsonify(dahua_alarm_cache)


@app.route('/ding_attendance')  # 钉钉考勤
def ding_attendance():
    global ding_attendance_cache
    if ding_attendance_cache is None:
        ding_attendance_cache = dapinapi.api_ding_attendance()
        return jsonify(ding_attendance_cache)
    else:
        return jsonify(ding_attendance_cache)


@app.route('/visitor')  # 访客
def visitor():
    global visitor_cache
    if visitor_cache is None:
        visitor_cache = dapinapi.api_visitor()
        return jsonify(visitor_cache)
    else:
        return jsonify(visitor_cache)


@app.route('/garage')  # 车库
def garage():
    global garage_cache
    if garage_cache is None:
        garage_cache = dapinapi.api_garage()
        return jsonify(garage_cache)
    else:
        return jsonify(garage_cache)

@app.route('/dahua_energy')  # 大华能源（实时查询-无缓存）
def dahua_energy():
    try:
        # 从请求参数中获取参数值
        param1 = request.args.get('param1')  # 假设参数名为param1
        param2 = request.args.get('param2')  # 假设参数名为param2
        print(param1)
        print(param2)
        # 检查是否所有必要的参数都存在
        if not param1 or not param2:
            return jsonify({'error': '请检查您的传参'}), 400
        # 传递参数调用函数
        dahua_energy_cache = dapinapi.api_dahua_departmentEnergy(param1, param2)
        return jsonify(dahua_energy_cache)
    except Exception as e:
        # 捕获异常并返回错误信息
        return jsonify({'error': str(e)}), 500


if __name__ == "__main__":
    try:
        print("启动更新线程...")
        threading.Thread(target=update_ding_information_cache).start()
        threading.Thread(target=update_ten_min_cache).start()
        print("启动Flask应用...")
        app.run(port=34558, host="0.0.0.0", debug=False)
    except Exception as e:
        print(f"主程序启动失败: {e}")
