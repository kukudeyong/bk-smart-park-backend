import json
import requests
import time
from dahuaapi import get_token_by_password
from chargingStationToken import get_ykccn_token

dd_clientId = "dingzjcszrnd9orqgbww"  # 钉钉应用appkey
dd_clientSecret = "GFIAXB_5SlqVTtiYcGbNPaJ79k_RMhAC2-DqHViKNpfW0kv5YHuIELfb-7Nfhswb"  # 钉钉应用appsecert


class Ding_acc:
    def __init__(self):
        self.headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
        }
        self.access_token = None
        self.access_time = None
        self.expires_in = None
        self.get_access_token()

    def update_access_token(self):
        req_url = f"https://oapi.dingtalk.com/gettoken?appkey={dd_clientId}&appsecret={dd_clientSecret}"
        ret = requests.get(url=req_url, headers=self.headers)
        ret = ret.content.decode("utf-8")
        ret = json.loads(ret)
        self.access_token = ret["access_token"]
        self.expires_in = ret["expires_in"] - 1000

    def get_access_token(self):
        if self.access_token is None:
            self.update_access_token()
            self.access_time = time.time()
            time.sleep(1)
        now = time.time()
        interval = int(now - self.access_time)
        if interval > self.expires_in:
            self.update_access_token()
            self.access_time = time.time()
            time.sleep(1)
        return self.access_token

ding_acc = Ding_acc()

def api_ding_information_detail(id):
    access_token = ding_acc.get_access_token()
    url = f"https://oapi.dingtalk.com/topapi/blackboard/get?access_token={access_token}"
    headers = {
        "Content-Type": "application/json",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
    }
    params = {
        "operation_userid": "6614576617848581",
        "blackboard_id": id  # 修改为接受id作为参数
    }
    ret = requests.post(url, headers=headers, json=params)
    ret_content = ret.content.decode('utf-8')
    ret_dict = json.loads(ret_content)
    
    if ret_dict["errcode"] == 0:
        return ret_dict['result']
    else:
        print(ret_dict["errmsg"])
        return False

def api_ding_information():
    access_token = ding_acc.get_access_token()
    url = f"https://oapi.dingtalk.com/topapi/blackboard/listtopten?access_token={access_token}"
    headers = {
        "Content-Type": "application/json",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
    }
    params = {
        "userid": "6614576617848581"
    }
    ret = requests.post(url, headers=headers, json=params)
    ret_dict = json.loads(ret.content.decode("utf-8"))
    print(ret_dict)
    blackboard_all = ret_dict.get("blackboard_list", [])  # 使用get()避免KeyError
    print(blackboard_all)
    
    enhanced_blackboard_list = []
    for item in blackboard_all:
        detail = api_ding_information_detail(item["id"])  # 为每个项目调用详情接口
        if detail:
            detailed_item = {**item, "details": detail}  # 合并原条目与详情
            enhanced_blackboard_list.append(detailed_item)
    
    return enhanced_blackboard_list

def api_dahua_departmentEnergy(minDate,maxDate):
    acc_token, refer_token = get_token_by_password("172.16.100.30:8320", "system", 'Bokang@8369')
    params = {
        "ascriptionName": "",
        "costCenterNameList": [],
        "departmentIdList": [],
        "detectorCode": '',
        "maxDate": maxDate,
        "meterCode": '',
        "minDate": minDate,
        "orgCode": "",
        "purposeNameList": [],
        "shareFlag": "",
        "statisticType": "MONTH"
    }
    headers = {
        "Authorization": "bearer " + acc_token,  # bearer 后面需要有一个空格，否则会报错
        "Content-Type": "application/json"
    }
    url = "https://172.16.100.30:8320/evo-apigw/evo-ecsis/1.0.0/board/departmentEnergy"
    ret = requests.post(url=url, headers=headers, json=params, verify=False)
    ret = json.loads(ret.content.decode("utf-8"))
    if ret['success'] is True:
        data = ret["data"]
        return data
    else:
        print("获取大华能源数据出错...")
        return False


def api_dahua_alarm():
    acc_token, refer_token = get_token_by_password("172.16.100.30:8320", "system", 'Bokang@8369')
    params = {
        "pageNum": 1,
        "pageSize": 20,
        "language": "zh-cn",
        "dbType": 0,
        "alarmStat":1,
        "handleStatList":[0],
    }
    headers = {
        "Authorization": "bearer " + acc_token,  # bearer 后面需要有一个空格，否则会报错
        "Content-Type": "application/json"
    }
    url = "https://172.16.100.30:8320/evo-apigw/evo-event/1.2.0/alarm-record/page"
    ret = requests.post(url=url, headers=headers, json=params, verify=False)
    ret = json.loads(ret.content.decode("utf-8"))
    if ret['success'] is True:
        data = ret["data"]["pageData"]
        return data
    else:
        print("获取大华报警出错...")
        return False


def api_ding_attendance():
    # startDate = "20240515"
    # endDate = "20240516"
    startDate = time.strftime("%Y%m%d", time.localtime(time.time() - 86400))
    endDate = time.strftime("%Y%m%d")
    serviceId = "API-0ae5fd4c-5007-4ee1-b8c6-8f6cddcaf519"
    userId = "6614576617848581"
    deptId = [['博康机电一工厂', "840518519"], ['博康机电二工厂', '840497570'], ['博康饰件', '840576476'], ['铜陵博康机电', "840700147"]]
    dept_attend_info = list()
    for i in deptId:
        url = f"https://api.dingtalk.com/v1.0/datacenter/generalDataServices?startDate={startDate}&serviceId={serviceId}&endDate={endDate}&userId={userId}&deptId={i[1]}"
        headers = {
            "Host": "api.dingtalk.com",
            "x-acs-dingtalk-access-token": ding_acc.get_access_token(),
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
        }
        ret = requests.get(url, headers=headers)
        ret = json.loads(ret.content.decode("utf-8"))
        dataList = ret["dataList"][0]
        attend_user_cnt_1d = dataList['attend_user_cnt_1d']
        super_dept_name = dataList['super_dept_name']
        dept_name = dataList['dept_name']
        mbr_cnt_std = dataList['mbr_cnt_std']
        attendance_tax = eval(attend_user_cnt_1d) / eval(mbr_cnt_std)
        dept_info = {'super_dept_name': super_dept_name, "dept_name": dept_name, "mbr_cnt_std": mbr_cnt_std,
                     "attend_user_cnt_1d": attend_user_cnt_1d, "attendance_tax": attendance_tax}
        dept_attend_info.append(dept_info)

    return dept_attend_info


def api_visitor():
    url = "https://visitorapi.qixuw.com/visit/get_all"
    headers = {
        "Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb3JwX3Byb2R1Y3QiOiJjYXIiLCJ0eXBlcyI6ImlzdiIsImNvcnBpZCI6ImRpbmcwMDVjZTEwZmQwNzM3ZmNhMzVjMmY0NjU3ZWI2Mzc4ZiIsImNvcnBfbmFtZSI6Ilx1ODI5Y1x1NmU1Nlx1NTM1YVx1NWViN1x1NjczYVx1NzUzNVx1NjcwOVx1OTY1MFx1NTE2Y1x1NTNmOCIsInVzZXJpZCI6IjY2MTQ1NzY2MTc4NDg1ODEiLCJ1bmlvbmlkIjoiZDB1SUJpU0NiSTlOZEFzV2t0MFRKbVFpRWlFIiwibmFtZSI6Ilx1Njg0Mlx1NTJjNyIsInN0YWZmX25hbWUiOiJcdTY4NDJcdTUyYzciLCJzdGFmZmlkIjoiNjYxNDU3NjYxNzg0ODU4MSIsImpvYl9udW1iZXIiOiIiLCJtb2JpbGUiOjE4MDEwNzQ1Nzg3LCJhZG1pbiI6MSwiZGVwdF9pZF9saXN0IjoiWzYzODY3OTE2MF0ifQ.CPfJH6e0gtcpnkt3-CCoHdfhK_g5fT8vKzNj7qhN-fg",
        "Content-Type": "application/json",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
    }
    params = {
        "download": 0,
        "per_page": 10,
        "page": 1
    }

    ret = requests.post(url=url, headers=headers, params=params)
    ret = json.loads(ret.content.decode("utf-8"))
    count = 0
    arrive_count = 0
    visitor_dict = dict()
    visitor_data = list()
    if ret["errcode"] == 0:
        ret = ret['result']
        for item in ret:
            count = count + item["count"]
            arrive_count = arrive_count + item['arrive_count']
            temp_data = item["data"]
            for i in temp_data:
                i_info = {
                    "name": i["name"],
                    "purpose": i["purpose"],
                    "start_time": i["start_time"],
                    "end_time": i["end_time"],
                    "status": i["status"],
                    "mobile": i["mobile"],
                    "address": i['address'],
                    "user_name": i["user_name"]
                }
                visitor_data.append(i_info)
        visitor_dict["count"] = count
        visitor_dict["arrive_count"] = arrive_count
        visitor_dict["result"] = visitor_data
        return visitor_dict
    else:
        print("获取访客详情失败...")
        return False


def api_garage():
    url = "https://carapi.qixuw.com/online/get_all"
    headers = {
        "Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb3JwX3R5cGUiOiJkdGFsayIsImNvcnBfcHJvZHVjdCI6ImNhciIsInR5cGVzIjoiaXN2IiwiY29ycGlkIjoiZGluZzAwNWNlMTBmZDA3MzdmY2EzNWMyZjQ2NTdlYjYzNzhmIiwiY29ycF9uYW1lIjoiXHU4MjljXHU2ZTU2XHU1MzVhXHU1ZWI3XHU2NzNhXHU3NTM1XHU2NzA5XHU5NjUwXHU1MTZjXHU1M2Y4IiwidXNlcmlkIjoiNjYxNDU3NjYxNzg0ODU4MSIsInVuaW9uaWQiOiJkMHVJQmlTQ2JJOU5kQXNXa3QwVEptUWlFaUUiLCJuYW1lIjoiXHU2ODQyXHU1MmM3Iiwic3RhZmZfbmFtZSI6Ilx1Njg0Mlx1NTJjNyIsInN0YWZmaWQiOiI2NjE0NTc2NjE3ODQ4NTgxIiwiam9iX251bWJlciI6IkJLWDAxMzMyIiwibW9iaWxlIjoxODAxMDc0NTc4NywiYWRtaW4iOjEsImRlcHRfaWRfbGlzdCI6Ils2Mzg2NzkxNjBdIiwiYXZhdGFyIjoiaHR0cHM6XC9cL3N0YXRpYy1sZWdhY3kuZGluZ3RhbGsuY29tXC9tZWRpYVwvbFFEUEQyek9SRVJpcUlQTkFyek5Bcnl3WTdueHRQaWI3dTRHbEpPODFFMS1BQV83MDBfNzAwLmpwZyJ9.BRqTU-LGXmw5qNL5U8rtNymNkTkffSD3zWJ8qjfrpwY",
        "Content-Type": "application/json",
    }
    ret = requests.post(url=url, headers=headers)
    ret = json.loads(ret.content.decode("utf-8"))
    garage_list = list()
    if ret["errcode"] == 0:
        ret = ret['result']
        for item in ret:
            id = item['id']
            title = item["title"]
            num = item["num"]
            all_car_list = item.get('list', [])
            userful_cat_list = list()
            for i in all_car_list:
                car_info = {
                    "car_title": i["car_type"],
                    "car_number": i["number"]
                }
                userful_cat_list.append(car_info)
            branch = {
                'id':id,
                'title':title,
                'num':num,
                'use':len(userful_cat_list),
                'result':userful_cat_list

            }
            garage_list.append(branch)
        return garage_list
    else:
        print("车库获取失败...")
        return False

def api_chargingStation():
    url = "https://www.ykccn.com/ompApi/api/omp/cpo/cpoWorkbench/gunStatusStatistics"
    ykccnToken = get_ykccn_token()
    print(ykccnToken)
    headers = {
        "HEAD_TOKEN": ykccnToken,
        "Host": "www.ykccn.com",
        "Connection": "keep-alive",
        "Cipher-Type": "2",
        "Authorization": "YKC-AUTHORIZATION",
        "Content-Type": "application/json",
        "Accept": "application/json, text/plain, */*",
        "Security_Code": "1733967985924",
        "Origin": "https://www.ykccn.com",
        "Referer": "https://www.ykccn.com/OMP/OprWork",
    }
    body = (
        "ZArqwEja7NV5/T2TS+WVFYDchb4r/MMx1MJz13DHjmtg5qIWZ2xdM8dM8wbo0A/yBuUdVx91veI11OdoMokUAW7PMhQ7Jf/fTu6IIk5f9Sz3XcHHgBK8GB5i0lT3nhMqUosqBdTT/fUkR57U6hsRud5EGFL4pd9KpLYRmrPdo7n/Fnaj5veGJ8zgdZjOZRAXImBY3+C2s+efElzz+sVImhPy7UEZmuWk9rRBg5AoluwNDIKOLVcZjmWdky/6P/4scticbmdgbFAt4AfNFXDeQjLceA1Me2nrgnUm8qQMudngFGAQk3SZzUoy1XwsKEcIQ62YOiaUW88J+9niWqOA9TVqsnYFbiVSMI21Z6/7DxlB7xBuar16yoqiNdTlHWQAGztyBDluItHVaUYUWfGtITTn5XKoyOJ1iWHkz6HBod5n0nuBkWz68B0TMerazefTyQT/FHak0Apas8+Glgp0ZFYoUUWeZH/lYPR+KuXLbiygjgjhJ72Fh/RqiDNkushnbi2xQq8la34cZGOuQHGWpSXoHz6eHLP/MtIHJRHt2LQneXbyadGiEXIE/NoCdGI031DJV6uGkDvzN3MRTgDHQSeLFKcqOW2E4A4PC2pp10EhCpsaknDnOnF27NCs9dG6wPWgxFVlkBW6jT575dWSQjKQ1Eztu42zQjBQgLquoPqx7ekK3nX1uaiUmqI6EDxAevOqzuX3GKEahpOWtTNPoP1mZp3F/dkF4haAg2SfHIwqGOcDt86AqbrEK8Q7wrcbrDCnaT1WXHW2QMcE3vzOe0JTmFgiOkWuhsxJMFQrJzPP+IEMB3g+9tGzefkuc63C6IqRZVOtn6jIYYVRq4+gtaFMj5RqW5nJwOz9RyzClxV/1ZOTR9fN/r7UzqDJdNBI9KI+7d0LSahkqBvIILWIpfKEirCpSAc3qRdglFUDbtQmN2XmapGbR7YlhzVfjeT/vTo6mzLm9IHm/04mE6nIZcai1Ph1O4Co+wP0QvCWzcVQl7bk1R1n5jt72ool2cMGZnBZnXu9BWLzSwp+eTopi4P+HgSQjRC0Z60ZbVK+te7wYuz35638p1wQTQN9z0tG3Ex7j33L7auQBhqOvcsWmfpNFMT+V6G4FhAunvS0C8Va7DkPCB1IxS5r94Drr7y5"
    )
    # 发送POST请求
    try:
        response = requests.post(url, headers=headers, json=body)
        response.raise_for_status()  # 检查请求是否成功
        data = response.json()  # 解析响应的JSON数据
        dynamicOrderData = api_queryDynamicOrderStatistics(ykccnToken,body)
        # 获取并显示数据
        data['header']['token'] = ykccnToken
        data['header']['body'] = body
        data['body']['OrderData'] = dynamicOrderData
        return data
    except requests.exceptions.RequestException as e:
        print("获取充电桩【状态】信息失败，Error:", e)

def api_queryDynamicOrderStatistics(token,body):
    url = "https://www.ykccn.com/ompApi/api/omp/cpo/overviewStatistics/queryDynamicOrderStatistics"
    ykccnToken = token
    headers = {
        "HEAD_TOKEN": ykccnToken,
        "Host": "www.ykccn.com",
        "Connection": "keep-alive",
        "Cipher-Type": "2",
        "Authorization": "YKC-AUTHORIZATION",
        "Content-Type": "application/json",
        "Accept": "application/json, text/plain, */*",
        "Security_Code": "1733967985924",
        "Origin": "https://www.ykccn.com",
        "Referer": "https://www.ykccn.com/OMP/OprWork",
    }
    # 发送POST请求
    try:
        response = requests.post(url, headers=headers, json=body)
        response.raise_for_status()  # 检查请求是否成功
        data = response.json()  # 解析响应的JSON数据
        return data['body']
    except requests.exceptions.RequestException as e:
        print("获取充电桩【总览】数据失败，Error:", e)

if __name__ == '__main__':
    ret = api_garage()
    print(ret)
    # ret2 = api_chargingStation()
    # print(ret2)
    # ret3 = api_dahua_alarm()
    # print(ret3)
    # ret4 = api_ding_information()
    # print(ret4)