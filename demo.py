import datetime


# 范围时间
start_time = datetime.datetime.strptime(str(datetime.datetime.now().date()) + '7:00', '%Y-%m-%d%H:%M')
# 开始时间
print(start_time)
end_time = datetime.datetime.strptime(str(datetime.datetime.now().date()) + '20:00', '%Y-%m-%d%H:%M')
# 结束时间
print(end_time)
# 当前时间
now_time = datetime.datetime.now()
# 方法一：
# 判断当前时间是否在范围时间内
if start_time < now_time < end_time:
    print("是在这个时间区间内")