import json
import requests
import base64
import urllib3
import M2Crypto


urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def get_public_key(host):
    url = f'https://{host}/evo-apigw/evo-oauth/1.0.0/oauth/public-key'
    resp = requests.get(url=url, verify=False)
    data = json.loads(resp.content)['data']
    public_key = data['publicKey']
    return public_key


def make_pem_from_string(public_key):
    header = '-----BEGIN PUBLIC KEY-----'
    content = '\n'.join([public_key[i:i+64] for i in range(0, len(public_key), 64)])
    footer = '-----END PUBLIC KEY-----'
    x509 = '\n'.join([header, content, footer])
    return x509


def public_encrypt(public_key, password):
    x509 = make_pem_from_string(public_key)
    bio = M2Crypto.BIO.MemoryBuffer(x509.encode('utf-8'))
    rsa_pub = M2Crypto.RSA.load_pub_key_bio(bio)
    ctxt_pub = rsa_pub.public_encrypt(password.encode(), M2Crypto.RSA.pkcs1_padding)
    ctxt64_pub = base64.b64encode(ctxt_pub).decode()
    return ctxt64_pub


def get_token_by_password(host="172.16.100.30", username="system", password="Bokang@8369", client_id='172.16.100.30_56676', client_secret='22fd197a-11e1-41b6-97d2-7cc5b4ff7b76'):
    url = f'https://{host}/evo-apigw/evo-oauth/1.0.0/oauth/extend/token/'
    public_key = get_public_key(host)
    encrypt = public_encrypt(public_key,password)
    data = {
        "grant_type": "password",
        "username": f"{username}",
        "password": f"{encrypt}",
        "client_id": f"{client_id}",
        "client_secret": f"{client_secret}",
        "public_key": f"{public_key}"
    }
    resp = requests.post(url=url, json=data, verify=False)
    print(resp.text)
    data = json.loads(resp.content)['data']

    access_token = data['access_token']
    refresh_token = data['refresh_token']
    return access_token, refresh_token

if __name__ == '__main__':
    get_token_by_password()